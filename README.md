# BREAKOUT JS

## Description

Breakout clone in JavaScript.

Based on video [tutorial](https://youtu.be/ec8vSKJuZTk?list=PL_S3JdlI7CZafysn9l1XL9IGHc0ZD1nDV&t=5445) by Ania Kubów from "[freeCodeCamp](https://www.freecodecamp.org)".

## Usage

Enter "index.html" path in browser address bar.


## Changes

TODO!

* ('derek' comments in code)
    * collison handling
    * etc.
* others (check 'TODO' doc)
    * classes
    * etc.


## Links

* [Ania Kubów YouTube channel](https://www.youtube.com/aniakubow)
* [Original GitHub code repository](https://github.com/kubowania/breakout)
