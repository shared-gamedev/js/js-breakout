//TODO: get from id instead of class
const grid = document.querySelector(".grid");
const scoreDisplay = document.querySelector("#score");

const blockWidth = 100;
const blockHeight = 20;
const boardWidth = 560;
const boardHeight = 300;

const userWidth = 100;
const userHeight = 20;

const ballDiameter = 20;

const userStart = [230, 10];
let currentPosition = userStart;

const ballStart = [270, 40];
let ballCurrentPosition = ballStart;

let score = 0;

let timerId;
let xDirection = 2;
let yDirection = 2;


class Block {
    constructor(xAxis, yAxis) {
        //TODO: use fields 'x'+'y' instead of array
        // => make a 'Point' class
        this.bottomLeft = [xAxis, yAxis];
        this.bottomRight = [xAxis + blockWidth, yAxis];
        this.topLeft = [xAxis, yAxis + blockHeight];
        this.topRight = [xAxis + blockWidth, yAxis + blockHeight];
    }
}

//TODO: generate array dynamically
// (using grid sizes, offsets, nb blocks, etc.)
const blocks = [
    new Block(10, 270),
    new Block(120, 270),
    new Block(230, 270),
    new Block(340, 270),
    new Block(450, 270),
    new Block(10, 240),
    new Block(120, 240),
    new Block(230, 240),
    new Block(340, 240),
    new Block(450, 240),
    new Block(10, 210),
    new Block(120, 210),
    new Block(230, 210),
    new Block(340, 210),
    new Block(450, 210)
]


function createBlock(xPos, yPos) {
    const block = document.createElement("div");
    block.classList.add("block");
    block.style.left = xPos + "px";
    block.style.bottom = yPos + "px";
    return block;
}

function addBlocks() {
    for (let i=0; i<blocks.length; ++i) {
        const xPos = blocks[i].bottomLeft[0];
        const yPos = blocks[i].bottomLeft[1];

        const block = createBlock(xPos, yPos);
        grid.appendChild(block);
    }
}

//TODO:make common class with position, draw, etc.
function drawUser() {
    user.style.left = currentPosition[0] + "px";
    user.style.bottom = currentPosition[1] + "px";
}

function drawBall() {
    ball.style.left = ballCurrentPosition[0] + "px";
    ball.style.bottom = ballCurrentPosition[1] + "px";
}

function moveUser(e) {
    switch(e.key) {
        case "ArrowLeft":
            if (currentPosition[0] > 0) {
                currentPosition[0] -= 10;
                drawUser();
            }
            break
        case "ArrowRight":
            if (currentPosition[0] < 560 - blockWidth) {
                currentPosition[0] += 10;
                drawUser();
            }
            break
    }
}


//TODO: move user in update function called at interval instead
document.addEventListener("keydown", moveUser);


addBlocks();

//TODO: rename to 'paddle' (+css)
const user = document.createElement("div");
user.classList.add("user");
drawUser();
grid.appendChild(user);


const ball = document.createElement("div");
ball.classList.add("ball");
drawBall();
grid.appendChild(ball);


function moveBall() {
    ballCurrentPosition[0] += xDirection;
    ballCurrentPosition[1] += yDirection;
    checkForCollisions();

    drawBall();
}

timerId = setInterval(moveBall, 30);

//derek: changed collision handling
function checkForCollisions() {
    // blocks collisions
    for (let i=0; i<blocks.length; ++i) {
        //derek: changed collision handling
        if ((ballCurrentPosition[0] + ballDiameter > blocks[i].bottomLeft[0] &&
             ballCurrentPosition[0] < blocks[i].bottomRight[0]) &&
            (ballCurrentPosition[1] + ballDiameter > blocks[i].bottomLeft[1] &&
             ballCurrentPosition[1] < blocks[i].topLeft[1])) {

            // determine correct bounce direction
            let xChange = 1;
            let yChange = 1;
            if (ballCurrentPosition[0] + ballDiameter - xDirection <= blocks[i].bottomLeft[0] ||
                ballCurrentPosition[0] - xDirection >= blocks[i].bottomRight[0]) {
                // was not already colliding on X => new X direction
                xChange = -1;
            }
            if (ballCurrentPosition[1] + ballDiameter - yDirection <= blocks[i].bottomLeft[1] ||
                ballCurrentPosition[1] - yDirection >= blocks[i].topLeft[1]) {
                // was not already colliding on Y => new Y direction
                yChange = -1;
            }
            changeDirection(xChange, yChange);

            //TODO: use instead a block class 'destroy'
            const allBlocks = Array.from(document.querySelectorAll(".block"));
            allBlocks[i].classList.remove("block");
            blocks.splice(i, 1);

            score++;
            scoreDisplay.innerHTML = score;

            if (blocks.length === 0) {
                clearInterval(timerId);
                console.log("WIN!");
                scoreDisplay.innerHTML = "WIN!"
                document.removeEventListener("keydown", moveUser);
            }

            // stop at first colision
            return
        }
    }

    // user collision
    if ((ballCurrentPosition[0] + ballDiameter > currentPosition[0] &&
         ballCurrentPosition[0] < currentPosition[0] + userWidth) &&
        (ballCurrentPosition[1] + ballDiameter > currentPosition[1] &&
         ballCurrentPosition[1] < currentPosition[1] + userHeight)) {
        //TODO: handle correct collisions (360 degrees)
        changeDirection(1, -1);
    }

    // death?
    //TODO: allow small offset? (<-offset?)
    if (ballCurrentPosition[1] < 0) {
        clearInterval(timerId);
        console.log("GAME OVER!");
        scoreDisplay.innerHTML = "Game Over!"
        document.removeEventListener("keydown", moveUser);
    }

    //TODO: make separate function for walls
    // wall collisions
    xChange = 1;
    yChange = 1;
    if (ballCurrentPosition[0] < 0 ||
        ballCurrentPosition[0] >= (boardWidth - ballDiameter)) {
        xChange = -1;
    }
    if (ballCurrentPosition[1] >= (boardHeight - ballDiameter)) {
        yChange = -1;
    }
    changeDirection(xChange, yChange);
}

//TODO: use booleans instead?
function changeDirection(xChange, yChange) {
    xDirection *= xChange;
    yDirection *= yChange;
}


