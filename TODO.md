
* general
    * [ ] Scale size to window (adaptive)
    * [ ] visuals
        * [x] use sprites
    * [ ] handle lives

* UI
    * [ ] fonts...
    * [ ] start/restart button
	* [ ] pause button

* css
    * [ ] use variables
        * [ ] sizes (ball, blocks, paddle, etc.)

* block
    * [ ] class
    * [ ] destroy animation
    * [ ] more than 1 hit, with different display for each status

* ball
    * [ ] start above paddle (not always center)
    * [ ] handle 360 degrees collisions with paddle
    * [ ] add randomness at start?
